package otus.courses.shlemov.spring.framework.developer02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import otus.courses.shlemov.spring.framework.developer02.properties.ConfigProperties;
import otus.courses.shlemov.spring.framework.developer02.service.ProcessService;

import java.util.Locale;

@SpringBootApplication
@EnableConfigurationProperties(ConfigProperties.class)
public class Application {

    public static void main(String[] args) {

        Locale.setDefault(Locale.forLanguageTag("ru-RU")); // Провоцирует вступление на русском, а вопросы на английском, т.к. у меня английская локаль по умолчанию.

        final ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        final ProcessService processService = context.getBean(ProcessService.class);
        processService.start();
    }
}
