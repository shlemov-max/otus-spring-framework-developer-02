package otus.courses.shlemov.spring.framework.developer02.benchmark;

/**
 * Аннотация для методов,
 * время выполнения которых следует измерить.
 */
public @interface BenchmarkGetQuestionList {
}
