package otus.courses.shlemov.spring.framework.developer02.benchmark;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

    @Before("target(otus.courses.shlemov.spring.framework.developer02.dao.QuestionDao)")
    public void log(JoinPoint joinPoint) throws Throwable {
        System.out.println("\nEntered to method:" + joinPoint.getSignature().getName());
    }
}
