package otus.courses.shlemov.spring.framework.developer02.dao;

import otus.courses.shlemov.spring.framework.developer02.domain.Question;

import java.util.List;

/**
 * DAO вопросов к тестированию.
 */
public interface QuestionDao {

    /**
     * Получить список вопросов теста.
     *
     * @return список вопросов теста
     */
    List<Question> getQuestionList();

}
