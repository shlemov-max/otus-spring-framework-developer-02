package otus.courses.shlemov.spring.framework.developer02.domain;

import com.opencsv.bean.CsvBindByPosition;

/**
 * Вопрос теста.
 */
public class Question {

    /**
     * Текст вопроса.
     */
    @CsvBindByPosition(position = 0, required = true)
    private String question;

    /**
     * Первый вариант ответа.
     */
    @CsvBindByPosition(position = 1)
    private String firstVariant;

    /**
     * Второй вариант ответа.
     */
    @CsvBindByPosition(position = 2)
    private String secondVariant;

    /**
     * Третий вариант ответа.
     */
    @CsvBindByPosition(position = 3)
    private String thirdVariant;

    /**
     * Номер правильного варианта ответа.
     */
    @CsvBindByPosition(position = 4)
    private int rightVariantNumber;


    @Override
    public String toString() {
        return String.format("* %s, ---  1) %s; 2) %s; 3) %s;",
                this.question,
                this.firstVariant,
                this.secondVariant,
                this.thirdVariant
        );
    }


    // Getters & setters ---

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getFirstVariant() {
        return firstVariant;
    }

    public void setFirstVariant(String firstVariant) {
        this.firstVariant = firstVariant;
    }

    public String getSecondVariant() {
        return secondVariant;
    }

    public void setSecondVariant(String secondVariant) {
        this.secondVariant = secondVariant;
    }

    public String getThirdVariant() {
        return thirdVariant;
    }

    public void setThirdVariant(String thirdVariant) {
        this.thirdVariant = thirdVariant;
    }

    public int getRightVariantNumber() {
        return rightVariantNumber;
    }

    public void setRightVariantNumber(int rightVariantNumber) {
        this.rightVariantNumber = rightVariantNumber;
    }
}
