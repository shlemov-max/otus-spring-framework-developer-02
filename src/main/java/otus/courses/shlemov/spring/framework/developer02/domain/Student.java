package otus.courses.shlemov.spring.framework.developer02.domain;

/**
 * Данные о студенте.
 */
public class Student {

    /**
     * Фамилия.
     */
    private String surname;

    /**
     * Имя.
     */
    private String name;


    // Constructors ---

    public Student(final String surname, final String name) {
        this.surname = surname;
        this.name = name;
    }


    // Getters & setters ---


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
