package otus.courses.shlemov.spring.framework.developer02.localization;

/**
 * Сервис локализованного источника сообщений.
 */
public interface MessageSourceService {

    /**
     * Получить локализованное сообщение.
     *
     * @param messageKey ключ сообщения
     * @param params     параметры сообщения
     * @return локализованное сообщение
     */
    public String getLocalizedMessage(String messageKey, String[] params);
}
