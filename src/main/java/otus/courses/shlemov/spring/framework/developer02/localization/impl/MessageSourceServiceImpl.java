package otus.courses.shlemov.spring.framework.developer02.localization.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import otus.courses.shlemov.spring.framework.developer02.localization.MessageSourceService;
import otus.courses.shlemov.spring.framework.developer02.properties.AppProps;

/**
 * Сервис локализованного источника сообщений.
 */
@Service
public class MessageSourceServiceImpl implements MessageSourceService {

    /**
     * Настройки и (вычисляемые) свойства приложения.
     */
    private final AppProps appProps;

    /**
     * Источник ообщений
     */
    private final MessageSource messageSource;

    // Constructors ---

    /**
     * @param appProps текущиe настройки и (вычисляемые) свойства приложения.
     * @param messageSource источник сообщений
     */
    @Autowired
    public MessageSourceServiceImpl(final AppProps appProps, final MessageSource messageSource) {
        this.appProps = appProps;
        this.messageSource = messageSource;
    }

    // Public methods ---

    /**
     * Получить локализованное сообщение.
     *
     * @param messageKey ключ сообщения
     * @param params     параметры сообщения
     * @return локализованное сообщение
     */
    @Override
    public String getLocalizedMessage(final String messageKey, final String[] params) {
        final String localizedMessage = this.messageSource
                .getMessage(messageKey, params, this.appProps.getCurrentLocale());
        return localizedMessage;
    }

    // Getters & setters ---
}
