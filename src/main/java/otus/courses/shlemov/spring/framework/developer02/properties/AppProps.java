package otus.courses.shlemov.spring.framework.developer02.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Locale;

/**
 * Настройки и (вычислимые) свойства приложения
 */
@Component
public class AppProps {

    /**
     * Текущий язык
     */
    private final String currentLanguage;

    /**
     * Текущая страна
     */
    private final String currentCountry;

    /**
     * Текущая локаль
     */
    private final Locale currentLocale;

    /**
     * Путь к файлу с вопросами теста и ответами без учёта локализации
     */
    private final String testCsvPath;

    /**
     * Путь к локализованному файлу с вопросами теста и ответами
     */
    private final String localizedCsvFilePath;

    // Constructors ---

    /**
     * Настройки приложения
     *
     * @param configProperties
     */
    public AppProps(@Autowired ConfigProperties configProperties) {

        this.currentLanguage = configProperties.getLanguage().toLowerCase();
        this.currentCountry = configProperties.getCountry().toUpperCase();
        this.testCsvPath = configProperties.getTest().getCsvPath();

        this.currentLocale = new Locale(this.currentLanguage.toLowerCase(), this.currentCountry.toUpperCase());
        this.localizedCsvFilePath = this.testCsvPath + "_" + currentLanguage.toLowerCase() + "_" + currentCountry.toUpperCase() + ".csv";

        // Проверка правильности настроек локали
        final InputStream testCsvInputStream = ClassLoader.getSystemResourceAsStream(this.localizedCsvFilePath);
        if (testCsvInputStream == null) {
            System.out.println(String.format("\nLocalization package '%s' was not found. Verify that your language and country settings are correct.\n",
                    this.localizedCsvFilePath));
            System.exit(1);
        }
    }

    // Getters ---

    public String getCurrentLanguage() {
        return currentLanguage;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public Locale getCurrentLocale() {
        return currentLocale;
    }

    public String getLocalizedCsvFilePath() {
        return localizedCsvFilePath;
    }
}
