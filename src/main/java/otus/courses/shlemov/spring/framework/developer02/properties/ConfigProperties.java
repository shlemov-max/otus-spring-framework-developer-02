package otus.courses.shlemov.spring.framework.developer02.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Настройки приложения
 * <p>
 * Сделано, как описано здесь:
 * https://www.baeldung.com/configuration-properties-in-spring-boot - Guide to @ConfigurationProperties in Spring Boot
 */
@Configuration
@PropertySource("classpath:application.yml")
@ConfigurationProperties
public class ConfigProperties {

    /**
     * Путь к файлу с вопросами теста и ответами без учёта локализации
     */
    public static class Test {

        /**
         * Путь к файлу с вопросами теста и ответами без учёта локализации
         */
        private String csvPath;

        // Getters & setters ---

        public String getCsvPath() {
            return csvPath;
        }

        public void setCsvPath(String csvPath) {
            this.csvPath = csvPath;
        }
    }


    // Properties ---

    /**
     * Текущий язык
     */
    private String language;

    /**
     * Текущая страна
     */
    private String country;

    /**
     * Путь к файлу с вопросами теста и ответами без учёта локализации
     */
    private Test test;

    // Getters & settres ---

    public String getLanguage() {
        return language;
    }

    public String getCountry() {
        return country;
    }

    public Test getTest() {
        return test;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
