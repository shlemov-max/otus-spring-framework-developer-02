package otus.courses.shlemov.spring.framework.developer02.service;

/**
 * Основной процесс.
 */
public interface ProcessService {

    /**
     * Запуск процесса.
     */
    void start();

}
