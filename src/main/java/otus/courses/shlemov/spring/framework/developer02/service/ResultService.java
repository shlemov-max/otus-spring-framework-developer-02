package otus.courses.shlemov.spring.framework.developer02.service;


import otus.courses.shlemov.spring.framework.developer02.domain.Student;

/**
 * Сервис вывода результатов тестирования.
 */
public interface ResultService {

    /**
     * Вывести результаты.
     *
     * @param student данные о студенте
     * @param mark    оценка
     */
    void printResults(Student student, int mark);

}
