package otus.courses.shlemov.spring.framework.developer02.service;


import otus.courses.shlemov.spring.framework.developer02.domain.Student;

/**
 * Сервис сбора данных о студенте.
 */
public interface StudentDataService {

    /**
     * Получить данные о студенте.
     *
     * @return данные о студенте
     */
    Student getStudent();

}
