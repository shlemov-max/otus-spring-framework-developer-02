package otus.courses.shlemov.spring.framework.developer02.service;

/**
 * Сервис тестирования.
 */
public interface TestService {

    /**
     * Провести тестирование и получить оценку.
     *
     * @return оценка
     */
    int getMark();

}
