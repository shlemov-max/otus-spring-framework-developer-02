package otus.courses.shlemov.spring.framework.developer02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import otus.courses.shlemov.spring.framework.developer02.domain.Student;
import otus.courses.shlemov.spring.framework.developer02.service.ProcessService;
import otus.courses.shlemov.spring.framework.developer02.service.ResultService;
import otus.courses.shlemov.spring.framework.developer02.service.StudentDataService;
import otus.courses.shlemov.spring.framework.developer02.service.TestService;

/**
 * Основной процесс.
 */
@Service("processService")
public class ProcessServiceImpl implements ProcessService {

    private final StudentDataService studentDataService;

    private final TestService testService;

    private final ResultService resultService;

    // Constructors ---

    /**
     * @param studentDataService сервис сбора данных о студенте
     * @param testService        серсис тестирования
     * @param resultService      сревис вывода результатов
     */
    @Autowired
    public ProcessServiceImpl(
            StudentDataService studentDataService,
            TestService testService,
            ResultService resultService) {

        this.studentDataService = studentDataService;
        this.testService = testService;
        this.resultService = resultService;
    }

    // Public methods --

    @Override
    public void start() {

        final Student student = this.studentDataService.getStudent();
        final int mark = this.testService.getMark();
        this.resultService.printResults(student, mark);
    }
}
