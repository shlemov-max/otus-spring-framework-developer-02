package otus.courses.shlemov.spring.framework.developer02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import otus.courses.shlemov.spring.framework.developer02.domain.Student;
import otus.courses.shlemov.spring.framework.developer02.localization.MessageSourceService;
import otus.courses.shlemov.spring.framework.developer02.service.ResultService;

/**
 * Сервис вывода результатов тестирования.
 */
@Service("resultService")
public class ResultServiceImpl implements ResultService {

    /**
     * Источник локализованных сообщений.
     */
    private final MessageSourceService messageSourceService;

    // Constructors ---

    /**
     * @param messageSourceService обёртка источника локализованных сообщений
     */
    @Autowired
    public ResultServiceImpl(final MessageSourceService messageSourceService) {
        this.messageSourceService = messageSourceService;
    }

    // Public methods ---

    /**
     * Вывести результаты.
     *
     * @param student данные о студенте
     * @param mark    оценка
     */
    @Override
    public void printResults(final Student student, final int mark) {

        // Ваша оценка
        final String message = this.messageSourceService.getLocalizedMessage(
                "your.mark.is",
                new String[]{
                        student.getName(),
                        student.getSurname(),
                        Integer.valueOf(mark).toString()});

        System.out.println(message);
    }
}
