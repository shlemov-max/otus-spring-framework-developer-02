package otus.courses.shlemov.spring.framework.developer02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import otus.courses.shlemov.spring.framework.developer02.domain.Student;
import otus.courses.shlemov.spring.framework.developer02.localization.MessageSourceService;
import otus.courses.shlemov.spring.framework.developer02.service.StudentDataService;

import java.util.Scanner;

/**
 * Сервис сбора данных о студенте.
 */
@Service("studentDataService")
public class StudentDataServiceImpl implements StudentDataService {

    /**
     * Источник локализованных сообщений.
     */
    private final MessageSourceService messageSourceService;

    // Constructors ---

    /**
     * @param messageSourceService обёртка источника локализованных сообщений.
     */
    @Autowired
    public StudentDataServiceImpl(final MessageSourceService messageSourceService) {
        this.messageSourceService = messageSourceService;
    }

    // Public methods ---

    /**
     * Получить данные о студенте.
     *
     * @return данные о студенте
     */
    @Override
    public Student getStudent() {

        final Scanner in = new Scanner(System.in);

        // Укажите своё имя и нажмите "Enter"
        final String enterYourName = this.messageSourceService.getLocalizedMessage("enter.your.name", new String[]{});
        System.out.print("\n" + enterYourName);

        final String name = in.next();

        // Укажите свою фамилию и нажмите "Enter"
        final String enterYourSurname = this.messageSourceService.getLocalizedMessage("enter.your.surname", new String[]{});
        System.out.print("\n" + enterYourSurname);

        final String surname = in.next();

        // Здравствуйте!
        final String hello = this.messageSourceService.getLocalizedMessage("hello", new String[]{name, surname});
        System.out.print("\n" + hello);

        // Сейчас Вам будет предложен тест. После каждого вопроса следует ввести номер правильного варианта и нажать "Enter"
        final String nowYouWillBeOfferedTest = this.messageSourceService.getLocalizedMessage("now.you.will.be.offered.test", new String[]{});
        System.out.print("\n" + nowYouWillBeOfferedTest);

        System.out.println("");

        final String begin = in.nextLine();

        final Student student = new Student(surname, name);
        return student;
    }
}
