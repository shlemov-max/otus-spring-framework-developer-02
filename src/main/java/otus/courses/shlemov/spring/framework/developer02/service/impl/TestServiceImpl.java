package otus.courses.shlemov.spring.framework.developer02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import otus.courses.shlemov.spring.framework.developer02.dao.QuestionDao;
import otus.courses.shlemov.spring.framework.developer02.domain.Question;
import otus.courses.shlemov.spring.framework.developer02.localization.MessageSourceService;
import otus.courses.shlemov.spring.framework.developer02.service.TestService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Сервис тестирования.
 */
@Service("testService")
public class TestServiceImpl implements TestService {

    /**
     * Сервис локализованного источника сообщений.
     */
    private final MessageSourceService messageSourceService;

    /**
     * DAO вопросов тестирования.
     */
    private final QuestionDao questionDao;

    // Constructors ---

    /**
     * @param messageSourceService сервис локализованного источника сообщений.
     * @param questionDao          DAO вопросов тестирования.
     */
    @Autowired
    public TestServiceImpl(MessageSourceService messageSourceService, final QuestionDao questionDao) {
        this.messageSourceService = messageSourceService;
        this.questionDao = questionDao;
    }

    // Public methods ---

    /**
     * Провести тестирование и получить оценку.
     *
     * @return оценка
     */
    @Override
    public int getMark() {

        final Scanner in = new Scanner(System.in);
        final List<Question> questionList = questionDao.getQuestionList();

        int mark = 0;
        int questionQuan = questionList.size();
        for (int i = 0; i < questionQuan; i++) {
            Question question = questionList.get(i);
            System.out.println("\n" + question.toString());
            try {
                String answerVariantNumberStr = in.next("[1-3]");
                int answerVariantNumber = Integer.valueOf(answerVariantNumberStr);
                if (answerVariantNumber == question.getRightVariantNumber()) {
                    // Правильный ответ
                    mark++;
                }
            } catch (InputMismatchException e) {

                //Ошибка ввода. Допустимо вводить только цифры от 1 до 3. Давайте попробуем ещё раз.
                System.out.println(this.messageSourceService.getLocalizedMessage("input.error.lets.try.again", new String[]{}));
                in.next();
                i--;
            }
        }
        return mark;
    }
}
